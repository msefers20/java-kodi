package lv.lvt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;

import javax.swing.JOptionPane;

public class Sefers_2uzd {

	static String failaNosaukums;
	
    static char[][] izveidot (char[][] masivs){
        for(int i=0; i<masivs.length; i++){
            for(int j=0; j<masivs[i].length; j++){
                masivs[i][j] = 'x';
            }
        }
        JOptionPane.showMessageDialog(null, "Mas�vs veiksm�gi izveidots!");
        izvadit(masivs);
        return masivs;
       
    }
    static void aizpildit(char[][] masivs){
        Random rand = new Random();
        for(int i=0; i<masivs.length; i++){
            for(int j=0; j<masivs[i].length; j++){
                masivs[i][j] = (char) (rand.nextInt(26)+65);
            }
        }
        JOptionPane.showMessageDialog(null, "Mas�vs veiksm�gi aizpild�ts.");
        izvadit(masivs);
    }
    
    static void izvadit(char[][] masivs){
        String masivaVirkne="";
        for(int i=0; i<masivs.length; i++){
            for(int j=0; j<masivs[i].length; j++){
                masivaVirkne = masivaVirkne+masivs[i][j]+" \t";
            }
            masivaVirkne = masivaVirkne+"\n";
        }
        JOptionPane.showMessageDialog(null, masivaVirkne);
    }
    static void kartot (char[][] masivs){
        int[] Masivs = new int[masivs.length*masivs[0].length];
        int skaititajs=0;
       
        for(int i=0; i<masivs.length; i++){
            for(int j=0; j<masivs[i].length; j++){
                Masivs[skaititajs] = (int) masivs[i][j];
                skaititajs++;
            }
        }
        int minSS, pagMain;
        for(int i=0; i<Masivs.length-1; i++){
            minSS = i;
            for(int j=i+1; j<Masivs.length; j++){
                if(Masivs[j]<Masivs[minSS]){
                    minSS = j;
                }
            }
            pagMain = Masivs[minSS];
            Masivs[minSS] = Masivs[i];
            Masivs[i] = pagMain;
           
        }
        skaititajs=0;
        for(int i=0; i<masivs.length; i++){
            for(int j=0; j<masivs[i].length; j++){
                masivs[i][j] = (char) Masivs[skaititajs];
                skaititajs++;
            }
        }
        izvadit(masivs);
    }
    static void saglabat(char[][] masivs){
        failaNosaukums = "Sefers";
        try{
            FileWriter fw = new FileWriter(failaNosaukums, true);
            PrintWriter raksta = new PrintWriter(fw);
            for(int i=0; i<masivs.length; i++){
                for(int j=0; j<masivs[i].length; j++){
                raksta.print(masivs[i][j]+" \t");
                }
                raksta.println();
            }
            raksta.println("_____________________");
            raksta.close();
            JOptionPane.showMessageDialog(null, "Burti ierakst�ti "+failaNosaukums+" fail�");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "K��me ierakstot fail�!", "K��ME", JOptionPane.ERROR_MESSAGE);
        }
    }
    static void nolasit(){
            String teksts, tekstaVirkne="";
            try{
            	FileReader fr = new FileReader(failaNosaukums);
                BufferedReader lasa = new BufferedReader(fr);
                while((teksts=lasa.readLine()) !=null){
                    tekstaVirkne = tekstaVirkne+teksts+" \n";
                }
                JOptionPane.showMessageDialog(null, tekstaVirkne);
                lasa.close();
               
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "K��me ierakstot fail�! ","K��ME",JOptionPane.ERROR_MESSAGE);
            }
        }
	public static void main(String[] args) {
		int izvele;
		char[][] masivs = null;
		do{
			izvele = Integer.parseInt(JOptionPane.showInputDialog("1 - Izveidot mas�vu | 2 - Aizpild�t mas�vu | 3 - Izvad�t mas�vu | 4 - K�rtot mas�vu | 5 - Saglab�t mas�vu | 6 - Nolas�t mas�vu | 777 - Lai aptur�tu"));
			switch(izvele){
            case 1:
                int r=0, k=0;
                do{
                	k = Integer.parseInt(JOptionPane.showInputDialog("Ievadi kolonnas skaitu"));
                    r = Integer.parseInt(JOptionPane.showInputDialog("Ievadi rindu skaitu"));
                }while(r<2 || k<2);
                masivs = new char[r][k];
                izveidot(masivs);
            break;
            case 2:
                aizpildit(masivs);
            break;
            case 3:
                izvadit(masivs);
            break;
            case 4:
            	kartot(masivs);
            break;	
            case 5:
            	saglabat(masivs);
            break;	
            case 6:
            	nolasit();
            break;
            case 777:
                JOptionPane.showMessageDialog(null, "Programma aptur�ta!");
            break;
            default:
                JOptionPane.showMessageDialog(null, "Darb�ba nepast�v!");
            break;
            }
           
		}while(izvele != 777);

	}

}
