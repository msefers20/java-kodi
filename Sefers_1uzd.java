package lv.lvt;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Sefers_1uzd {
	static Scanner scan = new Scanner (System.in);
	static DecimalFormat aizKomata = new DecimalFormat("0.##");
	public static void milimetros(){
		int rez, sk1, sk2 = 10;
		System.out.println("Ievadi skaitli kas j�parveido.");
		sk1 = scan.nextInt();
		rez = sk1 * sk2;
		System.out.println(sk1+"cm P�rveidojot milimetros ir "+rez+"mm");
	}
	public static void metros(){
		double rez, sk1, sk2 = 100;
		System.out.println("Ievadi skaitli kas j�parveido.");
		sk1 = scan.nextInt();
		rez = sk1 / sk2;
		System.out.println(sk1+"cm P�rveidojot metros ir "+rez+"m");
	}
	public static void collas(){
		double rez, sk1, sk2 = 2.54;
		System.out.println("Ievadi skaitli kas j�parveido.");
		sk1 = scan.nextInt();
		rez = sk1 / sk2;
		System.out.println(sk1+"cm P�rveidojot collas ir "+aizKomata.format(rez)+" collas");
	}
	public static void main(String[] args) {
		char izvele;
		do{
			
			System.out.println("1- P�rveidot milimetros | 2- P�rveidot metros | 3- P�rveidot coll�s | x-Aptur�t!");
			izvele = scan.next().charAt(0);
			izvele = Character.toLowerCase(izvele);
	
			switch(izvele){
			case '1': milimetros();		break;				
			case '2': metros(); 		break;		
			case '3': collas(); 		break;
			case 'x': System.out.println("Programma aptu�ta"); break;
			default: System.out.println("��da darb�ba nepast�v!");
			}
		}while(izvele != 'x');
		scan.close();
	}

}
